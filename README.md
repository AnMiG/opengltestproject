Some features (maybe disadvantages):

# OpenGL #
Used surface texture to render camera preview. Textured square was scaled with matrixes to fit screen (not in shader).

# Camera #
As the new camera API has came with API 21 i decided to create two classes: CameraStreamLollipop and CameraStreamKitkat to keep compability.

# Sensors #
As i had no device with magnetometer for testing - i did not use it. So i implemented only accelerometer sensor and the program does not define the azimuth. That`s why cube rotation might be strange sometimes.

# Text #
As the text in OpenGl is a large and complicated theme, i decided to use more simple way. Drawing text on canvas, and then copy it as the bitmap to texture. This way is bad for the performance, but in this project that will not cause great effect.

[Download APK](https://www.dropbox.com/s/vq9ksonayymm0q5/OpenGLTestProject.apk?dl=0)