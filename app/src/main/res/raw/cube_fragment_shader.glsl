precision mediump float;

uniform samplerCube u_Texture0;

varying vec3 v_Position;

void main() {
    gl_FragColor = textureCube(u_Texture0, v_Position);
}