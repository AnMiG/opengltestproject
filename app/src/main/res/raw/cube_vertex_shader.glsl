attribute vec4 a_Position;

uniform mat4 u_MVPMatrix;

varying vec3 v_Position;

void main() {
    v_Position = a_Position.xyz;
    gl_Position = u_MVPMatrix * a_Position;
}