package com.anmig.opengl_test_project.camera_rendering.utils;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

/**
 * Created by anmig on 25.06.2017.
 */

public class Cube {
    private FloatBuffer mVertexData;
    private ByteBuffer mIndexArray;
    private final float cubeSize = 1f;

    public Cube() {
        float s = cubeSize / 2f;
        float[] vertices = {
                // cube vertices
                -s, s, s,     // top left front
                s, s, s,     // top right front
                -s, -s, s,     // bottom left front
                s, -s, s,     // bottom right front
                -s, s, -s,     // top left back
                s, s, -s,     // top right back
                -s, -s, -s,     // bottom left back
                s, -s, -s      // bottom right back
        };

        mVertexData = ByteBuffer
                .allocateDirect(vertices.length * 4)
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer();
        mVertexData.put(vertices);

        mIndexArray = ByteBuffer.allocateDirect(36)
                .put(new byte[]{
                        // cube facets
                        // front
                        1, 3, 0,
                        0, 3, 2,

                        // back
                        4, 6, 5,
                        5, 6, 7,

                        // left
                        0, 2, 4,
                        4, 2, 6,

                        // right
                        5, 7, 1,
                        1, 7, 3,

                        // top
                        5, 1, 4,
                        4, 1, 0,

                        // bottom
                        6, 2, 7,
                        7, 2, 3
                });
        mIndexArray.position(0);
    }

    public FloatBuffer getVertexData() {
        return mVertexData;
    }

    public ByteBuffer getIndexArray() {
        return mIndexArray;
    }
}
