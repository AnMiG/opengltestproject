package com.anmig.opengl_test_project.camera_rendering.camera_stream;

import android.graphics.Point;
import android.graphics.SurfaceTexture;
import android.os.Handler;

/**
 * Created by anmig on 22.06.17.
 */

public interface ICameraStream {

    void openCamera(SurfaceTexture surfaceTexture, Handler backgroundHandler);

    void closeCamera();

    Point getPreviewSize();
}
