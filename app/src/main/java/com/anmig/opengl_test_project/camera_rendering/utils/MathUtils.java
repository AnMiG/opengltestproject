package com.anmig.opengl_test_project.camera_rendering.utils;

import android.graphics.Point;

import java.util.LinkedHashSet;
import java.util.Random;
import java.util.Set;

public class MathUtils {
    public static final float DEGREES_90 = MathUtils.degreesToRadians(90f);
    public static final float DEGREES_180 = MathUtils.degreesToRadians(180f);
    public static final float DEGREES_270 = MathUtils.degreesToRadians(270f);
    public static final float DEGREES_360 = MathUtils.degreesToRadians(360f);

    /**
     * Returns angle from 0 to 360 in radians
     *
     * @param
     * @return
     */
    public static float getNormalizedAngleInRadians(float angle) {
        if (angle <= 0)
            angle += DEGREES_360;
        if (angle > DEGREES_360)
            angle -= DEGREES_360;
        return angle;
    }

    /**
     * Returns angle from 0 to 360
     *
     * @param
     * @return
     */
    public static float getNormalizedAngleInDegrees(float angle) {
        if (angle <= 0)
            angle += 360;
        if (angle > 360)
            angle -= 360;
        return angle;
    }

    public static float getAngle(Point p1, Point p2) {
        double dx = (p2.x - p1.x);
        double dy = (p2.y - p1.y);
        return (float) Math.atan2(dy, dx);
    }

    public static float getAngle(float p1x, float p1y, float p2x, float p2y) {
        double dx = (p2x - p1x);
        double dy = (p2y - p1y);
        return (float) Math.atan2(dy, dx);
    }

    public static float getAngle(float x, float y) {
        return (float) Math.atan2(y, x);
    }

    public static float getDistance(Point p1, Point p2) {
        double dx = (p2.x - p1.x);
        double dy = (p2.y - p1.y);
        double alpha = Math.atan2(dy, dx);
        return (float) (dx / Math.cos(alpha));
    }

    public static float getDistance(float p1x, float p1y, float p2x, float p2y) {
        double dx = (p2x - p1x);
        double dy = (p2y - p1y);
        double alpha = Math.atan2(dy, dx);
        return (float) (dx / Math.cos(alpha));
    }

    public static float getDistance(float x, float y) {
        double alpha = Math.atan2(y, x);
        return (float) (x / Math.cos(alpha));
    }

//    public static Point getPointOnLineEnd(Point lineStartPoint, float lineLength, float angle) {
//        return getPointOnLineEnd(lineStartPoint.x, lineStartPoint.y, lineLength, angle);
//    }

//    public static Point getPointOnLineEnd(float startX, float startY, float lineLength, float angle) {
//        Point p = new Point();
//        p.x = startX + ((lineLength) * (float) Math.cos(angle));
//        p.y = startY + ((lineLength) * (float) Math.sin(angle));
//        return p;
//    }
//
//    public static Point getPointOnPerpendicular(Point linePoint, float angle, float perpendicularLength) {
//        Point p = new Point();
//        p.x = linePoint.x + (perpendicularLength * (float) Math.sin(angle));
//        p.y = linePoint.y - (perpendicularLength * (float) Math.cos(angle));
//        return p;
//    }

    public static float getVectorProjectionX(float distance, float angle) {
        return (float) (distance * Math.cos(angle));
    }

    public static float getVectorProjectionY(float distance, float angle) {
        return (float) (distance * Math.sin(angle));
    }

    public static float degreesToRadians(float gradus) {
        return (gradus * (float) Math.PI) / 180f;
    }

    public static float radiansToDegrees(float radians) {
        return (radians * 180f / (float) Math.PI);
    }

    public static double radiansToDegrees(double radians) {
        return (radians * 180d / Math.PI);
    }

    public static float randomFloat(float min, float max) {
        return (float) Math.random() * (max - min) + min;
    }

    public static double randomDouble(double min, double max) {
        return Math.random() * (max - min) + min;
    }

    public static int randomInt(int min, int max) {
        return (int) Math.floor(Math.random() * (max - min + 1)) + min;
    }

    /**
     * Returns the Set of indexes from th list (0 - max) without repetition
     *
     * @param max
     * @param numbersNeeded
     * @return
     */
    public static Set<Integer> randomWithoutRepetition(int max, int numbersNeeded) {
        Random rng = new Random(); // Ideally just createObject one instance globally
        // Note: use LinkedHashSet to maintain insertion order
        Set<Integer> generated = new LinkedHashSet<>();
        while (generated.size() < numbersNeeded) {
            Integer next = rng.nextInt(max) + 1;
            // As we're adding to a set, this will automatically do a containment check
            generated.add(next);
        }
        return generated;
    }

    /**
     * @param value - 1.445
     * @param ratio - 10, 100, 1000
     * @return - 1.5
     */
    public static float round(float value, int ratio) {
        return (float) (Math.rint(ratio * (double) value) / ratio);
    }


    /**
     * Converts sceonds to array [hours, mins, secs]
     *
     * @param seconds
     * @return
     */
    public static int[] convertSecondsToHourMinSec(Float seconds) {
        long longVal = seconds.longValue();
        int hours = (int) longVal / 3600;
        int remainder = (int) longVal - hours * 3600;
        int mins = remainder / 60;
        remainder = remainder - mins * 60;
        int secs = remainder;

        int[] ints = {hours, mins, secs};
        return ints;
    }

    /**
     * Converts sceonds to "00:00"
     *
     * @param pSeconds
     * @return
     */
    public static String convertSecondsToMinutesString(Float pSeconds) {
        long longVal = pSeconds.longValue();
        int minutes = (int) longVal / 60;
        int remainder = (int) longVal - minutes * 60;
        int seconds = remainder;

        String m = "" + minutes;
        if (minutes < 10)
            m = "0" + minutes;
        String s = "" + seconds;
        if (seconds < 10)
            s = "0" + seconds;
        return m + ":" + s;
    }
}
