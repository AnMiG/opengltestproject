package com.anmig.opengl_test_project.camera_rendering.utils;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

/**
 * Created by anmig on 25.06.2017.
 */

public class SensorListener implements SensorEventListener {
    private SensorManager mSensorManager;
    private Sensor mSensorAccelerometer;

    private float[] vector = new float[3];

    public SensorListener(Context context) {
        mSensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        mSensorAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
    }

    public void onResume() {
        mSensorManager.registerListener(this, mSensorAccelerometer, SensorManager.SENSOR_DELAY_FASTEST);
    }

    public void onPause() {
        mSensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        vector = event.values.clone();
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {
    }

    public float[] getVector() {
        return vector;
    }
}
