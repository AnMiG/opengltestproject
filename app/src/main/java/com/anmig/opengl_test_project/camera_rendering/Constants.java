package com.anmig.opengl_test_project.camera_rendering;

/**
 * Created by anmig on 21.06.17.
 */

public class Constants {
    public static final int BYTES_PER_FLOAT = 4;

    //UNIFORMS
    public static final String UNIFORM_MODEL_MATRIX = "u_ModelMatrix";
    public static final String UNIFORM_VIEW_MATRIX = "u_ViewMatrix";
    public static final String UNIFORM_PROJECTION_MATRIX = "u_ProjectionM";
    public static final String UNIFORM_ORIENTATION_MATRIX = "u_OrientationM";
    public static final String UNIFORM_MVP_MATRIX = "u_MVPMatrix";

    public static final String UNIFORM_ASPECT_RATIO = "u_AspectRatio";
    public static final String UNIFORM_COLOR = "u_Color";
    public static final String UNIFORM_EXTERNAL_OES_TEXTURE = "u_ExTexture";
    public static final String UNIFORM_TEXTURE_0 = "u_Texture0";
    public static final String UNIFORM_TEXTURE_1 = "u_Texture1";

    //ATTRIBUTES
    public static final String ATTRIBUTE_POSITION = "a_Position";
    public static final String ATTRIBUTE_COLOR = "a_Color";
    public static final String ATTRIBUTE_TEXTURE_COORD = "a_TexCoord";
}
