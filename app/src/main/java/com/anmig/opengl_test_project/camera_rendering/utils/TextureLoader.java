package com.anmig.opengl_test_project.camera_rendering.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLES20;
import android.opengl.GLUtils;
import android.support.annotation.NonNull;

/**
 * Created by anmig on 25.06.2017.
 */

public class TextureLoader {
    //   SCREEN COORDS:
    //
    //   -1,1-----|-----1,1
    //     |      |      |
    //     |______|______|
    //     |      |      |
    //     |      |      |
    //  -1,-1-----|-----1,1
    //
    //   TEXTURE COORDS:
    //
    //   0,0 ------------ > 1,0
    //   |   -------------
    //   |  |             |
    //   |  |             |
    //   |  |             |
    //   |  |_____________|
    //   \/
    //  0,1                1,1

    public static int loadTexture(Context context, int resourceId) {
        final int[] textureIds = new int[1];
        //create texture objects and push it to array
        //1 - textures count; 2 - array; 0 - array index to push
        GLES20.glGenTextures(1, textureIds, 0);
        if (textureIds[0] == 0) {
            return 0;
        }

        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = false;

        final Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), resourceId, options);

        if (bitmap == null) {
            //delete texture objects and remove it from array
            //1 - textures count; 2 - array; 0 - array index to remove from
            GLES20.glDeleteTextures(1, textureIds, 0);
            return 0;
        }

        //Activate the first unit (by default it is 0)
        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        //put the texture object to target GL_TEXTURE_2D of the unit GL_TEXTURE0
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureIds[0]);
        //put parameters to active textyre unit (1 - target; 2 - filter name; 3 - filter value)
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);

        //Put the bitmap to texture object
        GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bitmap, 0);

        // there is no need of uor bitmap as it is loaded to object
        bitmap.recycle();

        // reset target
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);

        return textureIds[0];
    }

    public static int loadTexture(@NonNull Bitmap bitmap) {
        final int[] textureIds = new int[1];
        GLES20.glGenTextures(1, textureIds, 0);

        if (textureIds[0] == 0) {
            return 0;
        }

        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureIds[0]);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
        GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bitmap, 0);
        bitmap.recycle();
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
        return textureIds[0];
    }

    public static int loadTextureCube(Context context, int[] resourceId) {
        final int[] textureIds = new int[1];
        //create texture objects and push it to array
        //1 - textures count; 2 - array; 0 - array index to push
        GLES20.glGenTextures(1, textureIds, 0);
        if (textureIds[0] == 0) {
            return 0;
        }

        // creation Bitmap
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = false;

        Bitmap[] bitmaps = new Bitmap[6];
        for (int i = 0; i < 6; i++) {
            bitmaps[i] = BitmapFactory.decodeResource(context.getResources(), resourceId[i], options);

            if (bitmaps[i] == null) {
                //delete texture objects and remove it from array
                //1 - textures count; 2 - array; 0 - array index to remove from
                GLES20.glDeleteTextures(1, textureIds, 0);
                return 0;
            }
        }

        //Activate the first unit (by default it is 0)
        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        //put the texture object to target GL_TEXTURE_2D of the unit GL_TEXTURE0
        GLES20.glBindTexture(GLES20.GL_TEXTURE_CUBE_MAP, textureIds[0]);

        //put parameters to active textyre unit (1 - target; 2 - filter name; 3 - filter value)
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_CUBE_MAP, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_CUBE_MAP, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);

        GLUtils.texImage2D(GLES20.GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 0, bitmaps[0], 0);
        GLUtils.texImage2D(GLES20.GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0, bitmaps[1], 0);

        GLUtils.texImage2D(GLES20.GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, 0, bitmaps[2], 0);
        GLUtils.texImage2D(GLES20.GL_TEXTURE_CUBE_MAP_POSITIVE_Y, 0, bitmaps[3], 0);

        GLUtils.texImage2D(GLES20.GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, 0, bitmaps[4], 0);
        GLUtils.texImage2D(GLES20.GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 0, bitmaps[5], 0);

        // there is no need of uor bitmap as it is loaded to object
        for (Bitmap bitmap : bitmaps) {
            bitmap.recycle();
        }

        // reset target
        GLES20.glBindTexture(GLES20.GL_TEXTURE_CUBE_MAP, 0);

        return textureIds[0];
    }
}
