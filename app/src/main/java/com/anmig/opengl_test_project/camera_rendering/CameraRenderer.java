package com.anmig.opengl_test_project.camera_rendering;

import android.app.Activity;
import android.content.res.Configuration;
import android.graphics.SurfaceTexture;
import android.opengl.GLES11Ext;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;

import com.anmig.opengl_test_project.R;
import com.anmig.opengl_test_project.camera_rendering.camera_stream.CameraStreamKitKat;
import com.anmig.opengl_test_project.camera_rendering.camera_stream.CameraStreamLollipop;
import com.anmig.opengl_test_project.camera_rendering.camera_stream.ICameraStream;
import com.anmig.opengl_test_project.camera_rendering.utils.Cube;
import com.anmig.opengl_test_project.camera_rendering.utils.SensorListener;
import com.anmig.opengl_test_project.camera_rendering.utils.ShaderWrapper;
import com.anmig.opengl_test_project.camera_rendering.utils.TextLoader;
import com.anmig.opengl_test_project.camera_rendering.utils.TextureLoader;
import com.anmig.opengl_test_project.camera_rendering.utils.MathUtils;
import com.anmig.opengl_test_project.camera_rendering.utils.Logger;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.text.DateFormat;
import java.util.Date;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 * Created by anmig on 22.06.2017.
 */

public class CameraRenderer implements GLSurfaceView.Renderer, SurfaceTexture.OnFrameAvailableListener {
    private CameraView mView;
    private SensorListener mSensor;

    private ICameraStream mCamera;
    private Cube mCube;

    private HandlerThread mBackgroundThread;
    private Handler mBackgroundHandler;
    private boolean mUpdateTexture;

    private ShaderWrapper mCameraShader;
    private ShaderWrapper mCubeShader;
    private ShaderWrapper mTextShader;

    private float[] mCameraViewMatrix = new float[16];
    private float[] mCameraProjectionMatrix = new float[16];
    private float[] mCameraModelMatrix = new float[16];
    private float[] mCameraMVPMatrix = new float[16];

    private float[] mCubeViewMatrix = new float[16];
    private float[] mCubeProjectionMatrix = new float[16];
    private float[] mCubeModelMatrix = new float[16];
    private float[] mCubeMVPMatrix = new float[16];

    private float[] mTextModelMatrix = new float[16];
    private float[] mTextMVPMatrix = new float[16];
    private float[] mTextViewMatrix = new float[16];
    private float[] mTextProjectionMatrix = new float[16];

    private SurfaceTexture mCameraTexture;
    private FloatBuffer mCameraVertices;
    private FloatBuffer mTextVertices;
    private int mCameraTextureHandle;
    private int mCubeTextureHandle;

    private int mSurfaceWidth = 0;
    private int mSurfaceHeight = 0;

    private float mEyeZ = 4.5f;
    private float mRatio = 1f;
    private float mLeft = -1f;
    private float mRight = 1f;
    private float mBottom = -1f;
    private float mTop = 1f;
    private float mWidth = 0;
    private float mHeight = 0;
    private float mNear = 1f;
    private float mFar = 10f;

    public CameraRenderer(CameraView cameraView) {
        mView = cameraView;
        mSensor = new SensorListener(mView.getContext());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            this.mCamera = new CameraStreamLollipop((Activity) mView.getContext());
        } else {
            this.mCamera = new CameraStreamKitKat((Activity) mView.getContext());
        }

        mCameraShader = new ShaderWrapper(mView.getContext());
        mCubeShader = new ShaderWrapper(mView.getContext());
        mTextShader = new ShaderWrapper(mView.getContext());
    }

    @Override
    public void onSurfaceCreated(GL10 glUnused, EGLConfig eglConfig) {
        GLES20.glClearColor(1f, 1f, 1f, 1f);
        GLES20.glEnable(GLES20.GL_DEPTH_TEST);

        mCameraShader.createProgram(R.raw.camera_vertex_shader, R.raw.camera_fragment_shader);
        mCubeShader.createProgram(R.raw.cube_vertex_shader, R.raw.cube_fragment_shader);
        mTextShader.createProgram(R.raw.text_vertex_shader, R.raw.text_fragment_shader);

        setViewMatrix(mCameraViewMatrix);
        setViewMatrix(mCubeViewMatrix);
        setViewMatrix(mTextViewMatrix);

        mCubeShader.useProgram();
        prepareCubeData();

        mTextShader.useProgram();
        prepareTextData();
    }

    @Override
    public void onSurfaceChanged(GL10 glUnused, int width, int height) {
        mSurfaceWidth = width;
        mSurfaceHeight = height;

        GLES20.glViewport(0, 0, mSurfaceWidth, mSurfaceHeight);

        setProjectionMatrix(mCameraProjectionMatrix);
        setProjectionMatrix(mCubeProjectionMatrix);
        setProjectionMatrix(mTextProjectionMatrix);

        mCameraShader.useProgram();
        prepareCameraData();

        mCamera.openCamera(mCameraTexture, mBackgroundHandler);
    }

    @Override
    public void onDrawFrame(GL10 glUnused) {
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);

        mCameraShader.useProgram();
        drawCamera();

        GLES20.glEnable(GLES20.GL_BLEND);
        GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);

        mCubeShader.useProgram();
        drawCube();

        mTextShader.useProgram();
        drawText();

        GLES20.glDisable(GLES20.GL_BLEND);
    }

    @Override
    public void onFrameAvailable(SurfaceTexture surfaceTexture) {
        mUpdateTexture = true;
        mView.requestRender();
    }

    public void onResume() {
        startBackgroundThread();
        mSensor.onResume();
    }

    public void onPause() {
        stopBackgroundThread();
        mUpdateTexture = false;
        mCamera.closeCamera();
        mSensor.onPause();
    }

    private void drawCamera() {
        Matrix.setIdentityM(mCameraModelMatrix, 0);
        Matrix.setIdentityM(mCameraModelMatrix, 0);
        setCameraModelMatrix();
        bindCameraMatrix();

        if (mUpdateTexture) {
            mCameraTexture.updateTexImage();
            mUpdateTexture = false;
        }

        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, 4);
    }

    private void drawCube() {
        Matrix.setIdentityM(mCubeModelMatrix, 0);
        setCubeModelMatrix();
        bindCubeMatrix();

        mCube.getVertexData().position(0);
        GLES20.glVertexAttribPointer(mCubeShader.getHandle(Constants.ATTRIBUTE_POSITION), 3, GLES20.GL_FLOAT, false, 0, mCube.getVertexData());
        GLES20.glEnableVertexAttribArray(mCubeShader.getHandle(Constants.ATTRIBUTE_POSITION));

        GLES20.glDrawElements(GLES20.GL_TRIANGLES, 36, GLES20.GL_UNSIGNED_BYTE, mCube.getIndexArray());
    }

    private void drawText() {
        Matrix.setIdentityM(mTextModelMatrix, 0);
        setTextModelMatrix();
        bindTextMatrix();

        int mTexture = TextLoader.createText(DateFormat.getDateTimeInstance().format(new Date()), 255, 0, 0, 255);
        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTexture);
        GLES20.glUniform1i(mTextShader.getHandle(Constants.UNIFORM_TEXTURE_0), 0);

        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, 4);
    }

    private void prepareCameraData() {
        //createCameraTexture
        int[] mTextureHandles = new int[1];
        GLES20.glGenTextures(1, mTextureHandles, 0);
        mCameraTextureHandle = mTextureHandles[0];
        GLES20.glBindTexture(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, mCameraTextureHandle);
        GLES20.glTexParameteri(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
        GLES20.glTexParameteri(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);
        GLES20.glTexParameteri(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
        GLES20.glTexParameteri(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
        SurfaceTexture oldSurfaceTexture = mCameraTexture;
        mCameraTexture = new SurfaceTexture(mCameraTextureHandle);
        mCameraTexture.setOnFrameAvailableListener(this);
        if (oldSurfaceTexture != null) {
            oldSurfaceTexture.release();
        }

        //Camera:
        float[] dataSquare = {
                -1f, 1f, 0f, 0f,

                1f, 1f, 1f, 0f,

                -1f, -1f, 0f, 1f,

                1f, -1f, 1f, 1f
        };
        mCameraVertices = ByteBuffer.allocateDirect(dataSquare.length * Constants.BYTES_PER_FLOAT).order(ByteOrder.nativeOrder()).asFloatBuffer();
        mCameraVertices.put(dataSquare);

        mCameraVertices.position(0);
        int positionAttributeLocation = mCameraShader.getHandle(Constants.ATTRIBUTE_POSITION);
        GLES20.glVertexAttribPointer(positionAttributeLocation, 2, GLES20.GL_FLOAT, false, 4 * Constants.BYTES_PER_FLOAT, mCameraVertices);
        GLES20.glEnableVertexAttribArray(positionAttributeLocation);

        mCameraVertices.position(2);
        int textureCoordsAttributeLocation = mCameraShader.getHandle(Constants.ATTRIBUTE_TEXTURE_COORD);
        GLES20.glVertexAttribPointer(textureCoordsAttributeLocation, 2, GLES20.GL_FLOAT, false, 4 * Constants.BYTES_PER_FLOAT, mCameraVertices);
        GLES20.glEnableVertexAttribArray(textureCoordsAttributeLocation);

        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        GLES20.glBindTexture(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, mCameraTextureHandle);
        GLES20.glUniform1i(mCameraShader.getHandle(Constants.UNIFORM_EXTERNAL_OES_TEXTURE), 0);
    }

    private void prepareCubeData() {
        mCube = new Cube();

        mCubeTextureHandle = TextureLoader.loadTextureCube(mView.getContext(), new int[]{
                R.drawable.side_1, R.drawable.side_2, R.drawable.side_3, R.drawable.side_4, R.drawable.side_5, R.drawable.side_6});

        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_CUBE_MAP, mCubeTextureHandle);
        GLES20.glUniform1i(mCubeShader.getHandle(Constants.UNIFORM_TEXTURE_0), 0);

    }

    private void prepareTextData() {
        float[] textVertices = {
                -1f, 1f, 0f, 0f,

                1f, 1f, 1f, 0f,

                -1f, -1f, 0f, 1f,

                1f, -1f, 1f, 1f
        };
        mTextVertices = ByteBuffer.allocateDirect(textVertices.length * Constants.BYTES_PER_FLOAT).order(ByteOrder.nativeOrder()).asFloatBuffer();
        mTextVertices.put(textVertices);

        mTextVertices.position(0);
        int positionAttributeLocation = mTextShader.getHandle(Constants.ATTRIBUTE_POSITION);
        GLES20.glVertexAttribPointer(positionAttributeLocation, 2, GLES20.GL_FLOAT, false, 4 * Constants.BYTES_PER_FLOAT, mTextVertices);
        GLES20.glEnableVertexAttribArray(positionAttributeLocation);

        mTextVertices.position(2);
        int textureCoordsAttributeLocation = mTextShader.getHandle(Constants.ATTRIBUTE_TEXTURE_COORD);
        GLES20.glVertexAttribPointer(textureCoordsAttributeLocation, 2, GLES20.GL_FLOAT, false, 4 * Constants.BYTES_PER_FLOAT, mTextVertices);
        GLES20.glEnableVertexAttribArray(textureCoordsAttributeLocation);
    }

    private void bindCameraMatrix() {
        Matrix.multiplyMM(mCameraMVPMatrix, 0, mCameraViewMatrix, 0, mCameraModelMatrix, 0);
        Matrix.multiplyMM(mCameraMVPMatrix, 0, mCameraProjectionMatrix, 0, mCameraMVPMatrix, 0);
        GLES20.glUniformMatrix4fv(mCameraShader.getHandle(Constants.UNIFORM_MVP_MATRIX), 1, false, mCameraMVPMatrix, 0);
    }

    private void bindCubeMatrix() {
        Matrix.multiplyMM(mCubeMVPMatrix, 0, mCubeViewMatrix, 0, mCubeModelMatrix, 0);
        Matrix.multiplyMM(mCubeMVPMatrix, 0, mCubeProjectionMatrix, 0, mCubeMVPMatrix, 0);
        GLES20.glUniformMatrix4fv(mCubeShader.getHandle(Constants.UNIFORM_MVP_MATRIX), 1, false, mCubeMVPMatrix, 0);
    }

    private void bindTextMatrix() {
        Matrix.multiplyMM(mTextMVPMatrix, 0, mTextViewMatrix, 0, mTextModelMatrix, 0);
        Matrix.multiplyMM(mTextMVPMatrix, 0, mTextProjectionMatrix, 0, mTextMVPMatrix, 0);
        GLES20.glUniformMatrix4fv(mTextShader.getHandle(Constants.UNIFORM_MVP_MATRIX), 1, false, mTextMVPMatrix, 0);
    }

    private void setViewMatrix(float[] matrix) {
        float eyeX = 0f;
        float eyeY = 0f;
        float eyeZ = mEyeZ;

        float centerX = 0;
        float centerY = 0;
        float centerZ = 0;

        float upX = 0f;
        float upY = 1f;
        float upZ = 0f;
        Matrix.setLookAtM(matrix, 0, eyeX, eyeY, eyeZ, centerX, centerY, centerZ, upX, upY, upZ);
    }

    private void setProjectionMatrix(float[] matrix) {
        mRatio = 1f;
        mLeft = -1f;
        mRight = 1f;
        mBottom = -1f;
        mTop = 1f;
        mNear = 1f;
        mFar = 10f;

        if (mSurfaceWidth > mSurfaceHeight) {
            mRatio = (float) mSurfaceWidth / mSurfaceHeight;
            mLeft *= mRatio;
            mRight *= mRatio;
        } else {
            mRatio = (float) mSurfaceHeight / mSurfaceWidth;
            mBottom *= mRatio;
            mTop *= mRatio;
        }

        mWidth = mRight * 2f;
        mHeight = mTop * 2f;

        Matrix.frustumM(matrix, 0, mLeft, mRight, mBottom, mTop, mNear, mFar);
    }

    private void setCubeModelMatrix() {
        float[] vector = mSensor.getVector();

        float alpha = (float) MathUtils.radiansToDegrees(Math.atan2(-vector[0], vector[1]));
        float betha = (float) MathUtils.radiansToDegrees(Math.atan2(vector[2], vector[1]));
        float hamma = (float) MathUtils.radiansToDegrees(Math.atan2(vector[0], vector[2]));

        if (betha < 0)
            hamma = 180f - hamma;

        Matrix.translateM(mCubeModelMatrix, 0, 0f, 0f, 2f);
        Matrix.rotateM(mCubeModelMatrix, 0, alpha, 0f, 0f, 1f);
        Matrix.rotateM(mCubeModelMatrix, 0, betha, 1f, 0f, 0f);
        Matrix.rotateM(mCubeModelMatrix, 0, hamma, 0f, 1f, 0f);
    }

    private void setCameraModelMatrix() {
        if (mView.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            Matrix.setRotateM(mCameraModelMatrix, 0, -90.0f, 0f, 0f, 1f);
        } else {
            Matrix.setRotateM(mCameraModelMatrix, 0, 0.0f, 0f, 0f, 1f);
        }
        float imgAspectRatio = mCamera.getPreviewSize().x / mCamera.getPreviewSize().y;
        float viewAspectRatio = mWidth / mHeight;
        float xScale = 1.0f;
        float yScale = 1.0f;
        if (imgAspectRatio > viewAspectRatio) {
            yScale = viewAspectRatio / imgAspectRatio;
        } else {
            xScale = imgAspectRatio / viewAspectRatio;
        }

        float alpha = (float) Math.atan(mTop / mNear);
        float b = (mEyeZ - mNear) * (float) Math.tan(alpha);
        float c = b + mTop;
        float scale = c * 2f;

        Matrix.scaleM(mCameraModelMatrix, 0, xScale * scale, yScale * scale, 0f);
    }

    private void setTextModelMatrix() {
        float alpha = (float) Math.atan(mTop / mNear);
        float a = (mEyeZ - mNear) * (float) Math.tan(alpha);
        float b = a + mTop;

        float betha = (float) Math.atan(mRight / mNear);
        float c = (mEyeZ - mNear) * (float) Math.tan(betha);
        float d = c + mRight;

        float scaleX = TextLoader.TEXT_WIDTH / TextLoader.TEXT_HEIGHT;
        Matrix.translateM(mTextModelMatrix, 0, -d + scaleX / 2f, b - 0.5f, 0.1f);
        Matrix.scaleM(mTextModelMatrix, 0, scaleX, 1f, 0f);
    }

    private void startBackgroundThread() {
        mBackgroundThread = new HandlerThread("CameraBackground");
        mBackgroundThread.start();
        mBackgroundHandler = new Handler(mBackgroundThread.getLooper());
    }

    private void stopBackgroundThread() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            mBackgroundThread.quitSafely();
        } else {
            mBackgroundThread.quit();
        }
        try {
            mBackgroundThread.join();
            mBackgroundThread = null;
            mBackgroundHandler = null;
        } catch (InterruptedException e) {
            Logger.e("CameraRenderer:stopBackgroundThread() - InterruptedException");
        }
    }
}
