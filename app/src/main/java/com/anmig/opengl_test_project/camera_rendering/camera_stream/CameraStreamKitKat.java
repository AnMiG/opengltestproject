package com.anmig.opengl_test_project.camera_rendering.camera_stream;

import android.app.Activity;
import android.content.res.Configuration;
import android.graphics.Point;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.os.Handler;

import java.io.IOException;
import java.util.List;

/**
 * Created by anmig on 22.06.17.
 */

public class CameraStreamKitKat implements ICameraStream {
    private Activity mActivity;
    private Point mPreviewSize;
    private Camera mCamera;

    public CameraStreamKitKat(Activity activity) {
        this.mActivity = activity;
        mPreviewSize = new Point(0, 0);
    }

    @Override
    public void openCamera(SurfaceTexture surfaceTexture, Handler backgroundHandler) {

        if (mCamera != null)
            closeCamera();
        mCamera = Camera.open();
        try {
            mCamera.setPreviewTexture(surfaceTexture);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

        calculatePreviewSize();
    }

    @Override
    public void closeCamera() {
        if (mCamera != null) {
            mCamera.stopPreview();
            mCamera.setPreviewCallback(null);
            mCamera.release();
        }
        mCamera = null;
    }

    @Override
    public Point getPreviewSize() {
        return mPreviewSize;
    }

    private void calculatePreviewSize() {
        Point windowSize = new Point();
        mActivity.getWindowManager().getDefaultDisplay().getSize(windowSize);

        int desiredSupportWidth;
        int desiredSupportHeight;
        if (mActivity.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            desiredSupportWidth = windowSize.y;
            desiredSupportHeight = windowSize.x;
        } else {
            desiredSupportWidth = windowSize.x;
            desiredSupportHeight = windowSize.y;
        }

        Camera.Parameters param = mCamera.getParameters();
        List<Camera.Size> supportSizesList = param.getSupportedPreviewSizes();
        int i;
        Point max = new Point(0, 0);
        for (i = 0; i < supportSizesList.size(); i++) {
            if (max.x < supportSizesList.get(i).width || max.y < supportSizesList.get(i).height)
                max.set(supportSizesList.get(i).width, supportSizesList.get(i).height);
            if (desiredSupportWidth == supportSizesList.get(i).width || desiredSupportHeight == supportSizesList.get(i).height) {
                break;
            }
        }
        if (i < supportSizesList.size()) {
            mPreviewSize.set(supportSizesList.get(i).width, supportSizesList.get(i).height);
        } else {
            mPreviewSize.set(max.x, max.y);
        }
        param.setPreviewSize(mPreviewSize.x, mPreviewSize.y);
        mCamera.setParameters(param);
        mCamera.startPreview();
    }
}
