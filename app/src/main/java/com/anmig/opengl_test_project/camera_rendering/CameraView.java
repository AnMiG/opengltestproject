package com.anmig.opengl_test_project.camera_rendering;

import android.content.Context;
import android.content.pm.PackageManager;
import android.opengl.GLSurfaceView;

/**
 * Created by anmig on 22.06.17.
 */

public class CameraView extends GLSurfaceView {
    public static final int CAMERA_PERMISSION = 101;
    private CameraRenderer mRenderer;

    public CameraView(Context context) {
        super(context);

        mRenderer = new CameraRenderer(this);

        setEGLContextClientVersion(2);
        setEGLConfigChooser(false);
        setRenderer(mRenderer);
        //THis mode renders only after calling GLSurfaceView.requestRender()
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
    }

    @Override
    public void onResume() {
        super.onResume();
        mRenderer.onResume();
    }

    @Override
    public void onPause() {
        mRenderer.onPause();
        super.onPause();
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == CAMERA_PERMISSION) {
            //Do nothing as onResume() will be called now
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //Permission granted
                //Do nothing as onResume() will be called now and camera will be reopened
            } else {
                //Permission denied
                //Do nothing as we cant do anything without camera
            }
        }
    }
}
