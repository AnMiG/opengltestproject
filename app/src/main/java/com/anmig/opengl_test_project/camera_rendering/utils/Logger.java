package com.anmig.opengl_test_project.camera_rendering.utils;

import android.util.Log;

import com.anmig.opengl_test_project.BuildConfig;

/**
 * Created by anmig on 18.06.2017.
 */

public class Logger {
    private final static String TAG = "AnMiG";
    private static StringBuilder str = new StringBuilder();

    public static void i(Object... messages) {
        if (!BuildConfig.DEBUG)
            return;
        str.setLength(0);
        for (Object m : messages) {
            str.append(m);
        }
        Log.i(TAG, str.toString());
    }

    public static void e(Object message) {
        if (!BuildConfig.DEBUG)
            return;
        str.setLength(0);
        str.append(message);
        Log.e(TAG, str.toString());
    }

    public static void e(Object message, Throwable exception) {
        if (!BuildConfig.DEBUG)
            return;
        str.setLength(0);
        str.append(message);
        Log.e(TAG, str.toString(), exception);
    }
}
