package com.anmig.opengl_test_project.camera_rendering.utils;

import android.content.Context;
import android.opengl.GLES20;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

public class ShaderWrapper {
    private Context mContext;
    private int mVertexShader = 0;
    private int mFragmentShader = 0;
    private int mShaderProgram = 0;
    private final HashMap<String, Integer> mShaderHandleMap = new HashMap<>();

    public ShaderWrapper(Context context) {
        this.mContext = context;
    }

    public int getHandle(String name) {
        if (mShaderHandleMap.containsKey(name)) {
            return mShaderHandleMap.get(name);
        }
        int handle = GLES20.glGetAttribLocation(mShaderProgram, name);
        if (handle == -1) {
            handle = GLES20.glGetUniformLocation(mShaderProgram, name);
        }
        if (handle == -1) {
            Logger.e("ShaderWrapper:getHandle() - Could not get attrib location for " + name);
        } else {
            mShaderHandleMap.put(name, handle);
        }
        return handle;
    }

    public int[] getHandles(String... names) {
        int[] res = new int[names.length];
        for (int i = 0; i < names.length; ++i) {
            res[i] = getHandle(names[i]);
        }
        return res;
    }

    public void createProgram(int vertexSource, int fragmentSource) {
        mVertexShader = createVertexShaderProgram(loadRawString(vertexSource));
        mFragmentShader = createFragmentShaderProgram(loadRawString(fragmentSource));
        mShaderProgram = linkShaderPrograms(mVertexShader, mFragmentShader);
        mShaderHandleMap.clear();
    }

    public void deleteProgram() {
        GLES20.glDeleteShader(mFragmentShader);
        GLES20.glDeleteShader(mVertexShader);
        GLES20.glDeleteProgram(mShaderProgram);
        mShaderProgram = 0;
        mVertexShader = 0;
        mFragmentShader = 0;
    }

    public void useProgram() {
        GLES20.glUseProgram(mShaderProgram);
    }

    private int createVertexShaderProgram(String vertexSource) {
        int vertexShaderHandle = GLES20.glCreateShader(GLES20.GL_VERTEX_SHADER);
        if (vertexShaderHandle != 0) {
            GLES20.glShaderSource(vertexShaderHandle, vertexSource);
            GLES20.glCompileShader(vertexShaderHandle);
            final int[] compileStatus = new int[1];
            GLES20.glGetShaderiv(vertexShaderHandle, GLES20.GL_COMPILE_STATUS, compileStatus, 0);
            if (compileStatus[0] == 0) {
                Logger.e("ShaderWrapper:createVertexShaderProgram() - Could not compile vertex shader: " + GLES20.glGetShaderInfoLog(vertexShaderHandle));
                GLES20.glDeleteShader(vertexShaderHandle);
                vertexShaderHandle = 0;
            }
        } else if (vertexShaderHandle == 0) {
            throw new RuntimeException("ShaderWrapper:createVertexShaderProgram() Error creating vertex shader.");
        }
        return vertexShaderHandle;
    }

    private int createFragmentShaderProgram(String fragmentSource) {
        int fragmentShaderHandle = GLES20.glCreateShader(GLES20.GL_FRAGMENT_SHADER);// Load in the fragment shader
        if (fragmentShaderHandle != 0) {
            GLES20.glShaderSource(fragmentShaderHandle, fragmentSource);// Pass in the shader source.
            GLES20.glCompileShader(fragmentShaderHandle);// Compile the shader.
            final int[] compileStatus = new int[1];// Get the compilation status.
            GLES20.glGetShaderiv(fragmentShaderHandle, GLES20.GL_COMPILE_STATUS, compileStatus, 0);
            if (compileStatus[0] == 0) {// If the compilation failed, delete the shader.
                Logger.e("ShaderWrapper Could not compile fragment shader: " + GLES20.glGetShaderInfoLog(fragmentShaderHandle));
                GLES20.glDeleteShader(fragmentShaderHandle);
                fragmentShaderHandle = 0;
            }
        } else if (fragmentShaderHandle == 0) {
            throw new RuntimeException("ShaderWrapper:createFragmentShaderProgram() - Error creating fragment shader.");
        }
        return fragmentShaderHandle;
    }

    private int linkShaderPrograms(int vertexShaderHandle, int fragmentShaderHandle) {
        int programHandle = GLES20.glCreateProgram();
        if (programHandle != 0) {
            GLES20.glAttachShader(programHandle, vertexShaderHandle);
            GLES20.glAttachShader(programHandle, fragmentShaderHandle);
            GLES20.glLinkProgram(programHandle);
            final int[] linkStatus = new int[1];
            GLES20.glGetProgramiv(programHandle, GLES20.GL_LINK_STATUS, linkStatus, 0);
            if (linkStatus[0] == 0) {// Если ссылку не удалось получить, удаляем программу.
                Logger.e("ShaderWrapper:linkShaderPrograms - Could not link shaders: " + GLES20.glGetShaderInfoLog(programHandle));
                GLES20.glDeleteProgram(programHandle);
                programHandle = 0;
            }
        } else if (programHandle == 0) {
            throw new RuntimeException("ShaderWrapper:linkShaderPrograms() - Error creating program.");
        }
        return programHandle;
    }

    private String loadRawString(int rawId) {
        InputStream is = mContext.getResources().openRawResource(rawId);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buf = new byte[1024];
        int len;
        try {
            while ((len = is.read(buf)) != -1) {
                baos.write(buf, 0, len);
            }
        } catch (IOException e) {
            Logger.e("ShaderWrapper:loadRawString() - Could not load shader resource");
        }
        return baos.toString();
    }
}
