package com.anmig.opengl_test_project.camera_rendering.utils;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;

/**
 * Created by anmig on 27.06.17.
 */

public class TextLoader {
    public static final int TEXT_WIDTH = 1024;
    public static final int TEXT_HEIGHT = 128;


    public static int createText(String text) {
        return createText(text, 64, 0, 0, 0, 255);
    }

    public static int createText(String text, int size) {
        return createText(text, size, 0, 0, 0, 255);
    }

    public static int createText(String text, int red, int green, int blue, int alpha) {
        return createText(text, 64, red, green, blue, alpha);
    }

    public static int createText(String text, int size, int red, int green, int blue, int alpha) {
        Bitmap bitmap = Bitmap.createBitmap(TEXT_WIDTH, TEXT_HEIGHT, Bitmap.Config.ARGB_4444);
        Canvas canvas = new Canvas(bitmap);
        bitmap.eraseColor(0);
        Paint textPaint = new Paint();
        textPaint.setTextSize(size);
        textPaint.setAntiAlias(true);
        textPaint.setARGB(alpha, red, green, blue);
        canvas.drawText(text, 28, TEXT_HEIGHT / 2 + size / 2, textPaint);
        int handle = TextureLoader.loadTexture(bitmap);
        bitmap.recycle();
        return handle;
    }
}
