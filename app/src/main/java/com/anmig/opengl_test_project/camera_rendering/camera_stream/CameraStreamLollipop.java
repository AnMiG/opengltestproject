package com.anmig.opengl_test_project.camera_rendering.camera_stream;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Point;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.util.Size;
import android.view.Surface;

import com.anmig.opengl_test_project.App;
import com.anmig.opengl_test_project.camera_rendering.CameraView;
import com.anmig.opengl_test_project.camera_rendering.utils.Logger;

import java.util.Arrays;
import java.util.concurrent.Semaphore;

/**
 * Created by anmig on 22.06.17.
 */

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class CameraStreamLollipop implements ICameraStream {
    private Activity mActivity;
    private CameraDevice mCameraDevice;
    private CameraCaptureSession mCaptureSession;
    private CaptureRequest.Builder mCaptureRequestBuilder;
    private String mCameraId;
    private Semaphore mCameraOpenCloseLock = new Semaphore(1);
    private Point mPreviewSize;

    public CameraStreamLollipop(Activity activity) {
        mActivity = activity;
        mPreviewSize = new Point(0, 0);
    }

    @Override
    public void openCamera(final SurfaceTexture surfaceTexture, final Handler backgroundHandler) {
        calculatePreviewSize();
        CameraManager cameraManager = (CameraManager) mActivity.getSystemService(Context.CAMERA_SERVICE);
        try {
            if (ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                if (!App.IS_PERMISSION_ASKED) {
                    ActivityCompat.requestPermissions(mActivity, new String[]{Manifest.permission.CAMERA}, CameraView.CAMERA_PERMISSION);
                    App.IS_PERMISSION_ASKED = true;
                }
                return;
            }
            cameraManager.openCamera(mCameraId, new CameraDevice.StateCallback() {
                @Override
                public void onOpened(@NonNull CameraDevice cameraDevice) {
                    mCameraOpenCloseLock.release();
                    mCameraDevice = cameraDevice;
                    createCameraPreviewSession(surfaceTexture, backgroundHandler);
                }

                @Override
                public void onDisconnected(@NonNull CameraDevice cameraDevice) {
                    mCameraOpenCloseLock.release();
                    mCameraDevice.close();
                    mCameraDevice = null;
                }

                @Override
                public void onError(@NonNull CameraDevice cameraDevice, int error) {
                    mCameraOpenCloseLock.release();
                    mCameraDevice.close();
                    mCameraDevice = null;
                }
            }, backgroundHandler);
        } catch (CameraAccessException e) {
            Logger.e("CameraStreamLollipop:openCamera() - CameraAccessException");
        }
    }

    @Override
    public void closeCamera() {
        try {
            mCameraOpenCloseLock.acquire();
            if (null != mCaptureSession) {
                mCaptureSession.close();
                mCaptureSession = null;
            }
            if (null != mCameraDevice) {
                mCameraDevice.close();
                mCameraDevice = null;
            }
        } catch (InterruptedException e) {
            throw new RuntimeException("CameraStreamLollipop:closeCamera() - Interrupted while trying to lock camera closing.", e);
        } finally {
            mCameraOpenCloseLock.release();
        }
    }

    private void createCameraPreviewSession(final SurfaceTexture surfaceTexture, final Handler backgroundHandler) {
        surfaceTexture.setDefaultBufferSize(mPreviewSize.x, mPreviewSize.y);
        Surface surface = new Surface(surfaceTexture);
        try {
            mCaptureRequestBuilder = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            mCaptureRequestBuilder.addTarget(surface);
            mCameraDevice.createCaptureSession(Arrays.asList(surface), new CameraCaptureSession.StateCallback() {
                @Override
                public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession) {
                    if (mCameraDevice == null) return;
                    mCaptureSession = cameraCaptureSession;
                    try {
                        mCaptureRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE);
                        mCaptureRequestBuilder.set(CaptureRequest.CONTROL_AE_MODE, CaptureRequest.CONTROL_AE_MODE_ON_AUTO_FLASH);

                        mCaptureSession.setRepeatingRequest(mCaptureRequestBuilder.build(), null, backgroundHandler);
                    } catch (CameraAccessException e) {
                        Logger.e("CameraStreamLollipop:createCameraPreviewSession() - CameraAccessException");
                    }
                }

                @Override
                public void onConfigureFailed(@NonNull CameraCaptureSession cameraCaptureSession) {

                }
            }, null);
        } catch (CameraAccessException e) {
            Logger.e("CameraStreamLollipop:createCameraPreviewSession() - CameraAccessException");
        }
    }

    @Override
    public Point getPreviewSize() {
        return mPreviewSize;
    }

    private void calculatePreviewSize() {
        Point windowSize = new Point();
        mActivity.getWindowManager().getDefaultDisplay().getRealSize(windowSize);

        int desiredSupportWidth;
        int desiredSupportHeight;
        if (mActivity.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            desiredSupportWidth = windowSize.y;
            desiredSupportHeight = windowSize.x;
        } else {
            desiredSupportWidth = windowSize.x;
            desiredSupportHeight = windowSize.y;
        }

        CameraManager cameraManager = (CameraManager) mActivity.getSystemService(Context.CAMERA_SERVICE);
        try {
            for (int i = 0; i < cameraManager.getCameraIdList().length; i++) {
                String camId = cameraManager.getCameraIdList()[i];

                CameraCharacteristics characteristics = cameraManager.getCameraCharacteristics(camId);
                if (characteristics.get(CameraCharacteristics.LENS_FACING) == CameraCharacteristics.LENS_FACING_FRONT)
                    continue;

                mCameraId = camId;
                StreamConfigurationMap map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
                Size[] supportSizesList = map.getOutputSizes(SurfaceTexture.class);


                int j;
                Point max = new Point(0, 0);
                for (j = 0; j < supportSizesList.length; j++) {
                    if (max.x < supportSizesList[j].getWidth() || max.y < supportSizesList[j].getHeight())
                        max.set(supportSizesList[j].getWidth(), supportSizesList[j].getHeight());
                    if (desiredSupportWidth == supportSizesList[j].getWidth() || desiredSupportHeight == supportSizesList[j].getHeight()) {
                        break;
                    }
                }

                if (j < supportSizesList.length) {
                    mPreviewSize.set(supportSizesList[j].getWidth(), supportSizesList[j].getHeight());
                } else {
                    mPreviewSize.set(max.x, max.y);
                }
            }
        } catch (CameraAccessException e) {
            Logger.e("CameraStreamLollipop:calculatePreviewSize() - CameraAccessException");
        }
    }
}
